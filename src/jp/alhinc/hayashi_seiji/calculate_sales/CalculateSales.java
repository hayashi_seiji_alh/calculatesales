package jp.alhinc.hayashi_seiji.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {
	//meinメソッド
	public static void main(String[] args) {
		if(args.length == 0 || 2 <= args.length ){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//支店定義ファイルの読み込み
		HashMap<String,String> branchList = new HashMap<String,String>();
		HashMap<String,Long> saleList = new HashMap<String,Long>();
		if(!(fileIn(args[0],"branch.lst",branchList,saleList))){
			return;
		}
		//売上ファイルの読み込み処理
		File cmdFiles = new File(args[0]);
		File[] cmdList = cmdFiles.listFiles();
		ArrayList<String> fileName = new ArrayList<String>();
		for(int i = 0; i<cmdList.length;i++){
			if(cmdList[i].getName().matches("^[0-9]{8}.rcd$")&& cmdList[i].isFile()){
				fileName.add(cmdList[i].getName());
			}
		}
		for(int i=0;i<fileName.size();i++){
			if(Integer.parseInt(fileName.get(i).substring(0,8)) != (Integer.parseInt(fileName.get(0).substring(0,8))+i)){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		BufferedReader brSaleList = null;
		for(int i = 0 ; i < fileName.size() ; i++ ){
			ArrayList<String> saleFileData = new ArrayList<String>();
			try{
				File file = new File(args[0],fileName.get(i));
				FileReader fr = new FileReader(file);
				brSaleList = new BufferedReader(fr);
					String readSaleList;
				while((readSaleList = brSaleList.readLine()) != null){
					saleFileData.add(readSaleList);
				}
					if(!(branchList.containsKey(saleFileData.get(0)))){
					System.out.println(fileName.get(i)+"の支店コードが不正です");
					return;
				}else if(!(saleFileData.size() == 2)){
					System.out.println(fileName.get(i)+"のフォーマットが不正です");
					return;
				}else if(!(saleFileData.get(1).matches("^[0-9]+$"))){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}else if(saleList.get(saleFileData.get(0))+ Long.parseLong(saleFileData.get(1)) >= 10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//マップへの格納および同一キーへの加算処理
				saleList.put( saleFileData.get(0) ,saleList.get(saleFileData.get(0))+ Long.parseLong(saleFileData.get(1)));
			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally{
				if(brSaleList != null){
					try{
						brSaleList.close();
					}catch(IOException e){
						System.out.println("closeできませんでした");
						return;
					}
				}
			}
		}
		//ファイルの作成～書き込み
		if(!(fileOut(args[0],"branch.out",branchList,saleList))){
			return;
		}
	}

	//ファイルの読み込みメソッド
	public static boolean fileIn(String filePath , String fileName,HashMap<String,String> map1,HashMap<String,Long> map2){
		BufferedReader brBranchList = null;
		try {
			File fileBranchList = new File(filePath , fileName);
			if(!(fileBranchList.exists())){
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader frBranchList = new FileReader(fileBranchList);
			brBranchList = new BufferedReader(frBranchList);
			String readBranchList;
			while((readBranchList = brBranchList.readLine()) != null){
				String[] splitedBranchList = readBranchList.split(",");
				if(!(splitedBranchList[0].matches("[0-9]{3}")) || !(splitedBranchList.length == 2)){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				map1.put(splitedBranchList[0],splitedBranchList[1]);
				map2.put(splitedBranchList[0], 0L);
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(brBranchList != null){
				try{
					brBranchList.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
		//ファイルの作成～書き込みメソッド
	public static boolean fileOut(String filePath, String fileName, HashMap<String,String> map1,HashMap<String,Long> map2){
		File newFile = new File(filePath , fileName);
		PrintWriter pw = null;
		BufferedWriter bw= null;
		try{
			newFile.createNewFile();
			FileWriter fw = new FileWriter(newFile);
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);
			for(String num1 : map1.keySet()){
				 String num2 = map1.get(num1) ;
				 long num3 = map2.get(num1);
				pw.println(num1+","+num2+","+num3);
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false ;
		}finally{
			if(bw != null){
				try{
					bw.close();
					pw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false ;
				}
			}
		}
		return true;
	}
}